package io.swagger.api;

import java.util.HashMap;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.model.Atendimento;
import io.swagger.model.PeriodoConta;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-01-11T13:05:36.799Z")

@Api(value = "greenws", description = "API referente a consulta de informações de atendimentos na MV")
public interface GreenApi {
	
	@ApiOperation(value = "Retorna um atendimento e informações correlatas", notes = "Retorna um atendimento e informações correlatas.", response = Atendimento.class, responseContainer = "List", tags = {
			"Atendimento", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um atendimento e informações correlatas.", response = Atendimento.class),
			@ApiResponse(code = 200, message = "Unexpected error", response = Atendimento.class) })
	@RequestMapping(value = "/obterAtendimento", produces = { "application/json" }, method = RequestMethod.GET)
	ResponseEntity<HashMap<String, Object>> getAtendimento(
			@ApiParam(value ="Código do Atendimento.") @RequestParam(value = "codigoAtendimento", required = false) Long codigoAtendimento,
			@ApiParam(value ="Código do Paciente.") @RequestParam(value = "codigoPaciente", required = false) Long codigoPaciente);
	
	@ApiOperation(value = "Retorna o período de uma conta.", notes = "Retorna o período de uma conta.", response = PeriodoConta.class, responseContainer = "List", tags = {
			"PeriodoConta", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna o período de uma conta.", response = PeriodoConta.class),
			@ApiResponse(code = 200, message = "Unexpected error", response = PeriodoConta.class) })
	@RequestMapping(value = "/obterPeriodoConta", produces = { "application/json" }, method = RequestMethod.GET)
	ResponseEntity<HashMap<String, Object>> getPeriodoConta(
			@ApiParam(value ="Código do Atendimento.") @RequestParam(value = "codigoAtendimento", required = false) Long codigoAtendimento,
			@ApiParam(value ="Número da conta.") @RequestParam(value = "numeroConta", required = false) Long numeroConta);
	
	
}
