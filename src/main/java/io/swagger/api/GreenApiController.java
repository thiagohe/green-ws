package io.swagger.api;

import java.io.ByteArrayInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.mv.foundation.controller.GenericSqlController;
import br.com.mv.foundation.util.ConsultaQuery;
import br.com.mv.integra.framework.FachadaFramework;
import br.com.mv.integra.framework.exception.InternacionalizacaoNaoEncontradaException;
import br.com.mv.integra.framework.exception.VersaoInvalidaException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.Swagger2SpringBoot;
import io.swagger.annotations.ApiParam;
import io.swagger.api.exception.ValidacaoException;

import io.swagger.model.Atendimento;
import io.swagger.model.PeriodoConta;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-01-11T13:05:36.799Z")

@Controller
@SuppressWarnings({ "unused", "rawtypes","unchecked", "static-access" })
public class GreenApiController implements GreenApi {
	
	private static FachadaFramework fachada;
	
	private static FachadaFramework getFachada()
			throws InternacionalizacaoNaoEncontradaException, VersaoInvalidaException {
		if (GreenApiController.fachada == null)
			GreenApiController.fachada = new FachadaFramework(1, new Locale("pt", "BR"), Swagger2SpringBoot.logger);
		return GreenApiController.fachada;
	}
	
	@Override
	public ResponseEntity<HashMap<String, Object>> getAtendimento(
			@ApiParam(value ="Código do Atendimento.") @RequestParam(value = "codigoAtendimento", required = false) Long codigoAtendimento,
			@ApiParam(value ="Código do Paciente.") @RequestParam(value = "codigoPaciente", required = false) Long codigoPaciente) {
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
     	ResponseEntity<HashMap<String,Object>> resultado;
     	HashMap<String,Object> retornoFormatado = new HashMap<String, Object>();
     	
     	Atendimento atendimento = null;
     	
     	try {
     		if (codigoAtendimento==null && codigoPaciente==null) {
         		httpStatus = HttpStatus.NOT_FOUND; 
         		throw new Exception();
         	}
     		atendimento = (Atendimento) getGenericSqlController().get(ConsultaQuery.getAtendimento(codigoAtendimento, codigoPaciente), Atendimento.class);
     		if (atendimento!=null) {
     			httpStatus = HttpStatus.OK;
     		}
     		else {
     			httpStatus = HttpStatus.NOT_FOUND;
     		}
     	} catch(Exception e) {
 			e.printStackTrace();
     	} finally {
     		retornoFormatado.put("atendimento", atendimento);
 			resultado = new ResponseEntity<HashMap<String,Object>>(retornoFormatado, httpStatus);
     	}
     	
     	return resultado;
	}
	

	private static GenericSqlController genericSqlController;
	
	private synchronized GenericSqlController getGenericSqlController() {
		if (genericSqlController==null) {
			genericSqlController = new GenericSqlController();
		}
		return genericSqlController;
	}

	@Override
	public ResponseEntity<HashMap<String, Object>> getPeriodoConta(
			@ApiParam(value ="Código do Atendimento.") @RequestParam(value = "codigoAtendimento", required = false) Long codigoAtendimento,
			@ApiParam(value ="Número da conta.") @RequestParam(value = "numeroConta", required = false) Long numeroConta) {
		
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
     	ResponseEntity<HashMap<String,Object>> resultado;
     	HashMap<String,Object> retornoFormatado = new HashMap<String, Object>();
     	
     	PeriodoConta periodoConta = null;
     	
     	try {
     		if (codigoAtendimento==null || numeroConta==null) {
         		httpStatus = HttpStatus.NOT_FOUND; 
         		throw new Exception();
         	}
     		
     		periodoConta = (PeriodoConta) getGenericSqlController().get(ConsultaQuery.getPeriodoConta(codigoAtendimento, numeroConta), PeriodoConta.class);
     		if (periodoConta!=null) {
     			httpStatus = HttpStatus.OK;
     		}
     		else {
     			httpStatus = HttpStatus.NOT_FOUND;
     		}
     	} catch(Exception e) {
 			e.printStackTrace();
     	} finally {
     		retornoFormatado.put("periodoConta", periodoConta);
 			resultado = new ResponseEntity<HashMap<String,Object>>(retornoFormatado, httpStatus);
     	}
     	
     	return resultado;
	}
	
	


}