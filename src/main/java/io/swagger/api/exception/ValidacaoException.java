package io.swagger.api.exception;

public class ValidacaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1537415742149473363L;
	public ValidacaoException() { }
	
	public ValidacaoException(String message) {
		super(message);
	}
}
