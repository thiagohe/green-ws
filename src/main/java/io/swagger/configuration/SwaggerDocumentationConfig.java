package io.swagger.configuration;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.Swagger2SpringBoot;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-06-13T10:33:22.799Z")

@Configuration
public class SwaggerDocumentationConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("Informações sobre API Green - MV")
            .description("Serviços para obtenção de informações de atendimentos, contas e pacientes.")
            .license("")
            .licenseUrl("")
            .termsOfServiceUrl("")
            .version("1.0.0")
            .contact(new Contact("","", ""))
            .build();
    }

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("io.swagger.api"))
                    .build()
                .apiInfo(apiInfo());
    }

    @Bean
    public DataSource dataSource() {
    	Integer maxIdle;
        String paramMaxIdle, paramMaxAge;
        Long maxAge;
        
    	DataSource dataSource = new DataSource();
        dataSource.setDriverClassName(Swagger2SpringBoot.context.getInitParameter("jintegra.db.driver"));
        dataSource.setUrl(Swagger2SpringBoot.context.getInitParameter("jintegra.db.url"));
        dataSource.setUsername(Swagger2SpringBoot.context.getInitParameter("jintegra.db.user"));
        dataSource.setPassword(Swagger2SpringBoot.context.getInitParameter("jintegra.db.password"));
        
        paramMaxIdle = Swagger2SpringBoot.context.getInitParameter("jintegra.db.maxIdleTime");
        
        if(paramMaxIdle!= null) {
            maxIdle = new Integer(paramMaxIdle); 
        } else {
           try {
        	   maxIdle = new Integer(1800);
           } catch(Exception e) {
        	   maxIdle = new Integer(1800);
           }
        }
        
        dataSource.setMaxIdle(maxIdle);
        
        paramMaxAge = Swagger2SpringBoot.context.getInitParameter("jintegra.db.maxConnectionAge");
        
        if(paramMaxAge != null) {
        	try {
        		maxAge = new Long(paramMaxAge);
        	} catch (Exception e) {
        		maxAge = new Long(1800);
        	}
        } else {
        	maxAge = new Long(1800);
        }
        
        dataSource.setMaxAge(maxAge);
        
        return dataSource;
    }
}
