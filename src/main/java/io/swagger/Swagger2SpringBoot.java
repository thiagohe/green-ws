package io.swagger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import io.swagger.api.TokenFilter;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "io.swagger", "br.com.mv.integra.repositorio" })
public class Swagger2SpringBoot extends SpringBootServletInitializer {

	public static Logger logger = Logger.getLogger("LOG_ENVIAR");
	public static ServletContext context;
	
	@Override
    public void onStartup(ServletContext servletContext)
            throws ServletException {
		if(Swagger2SpringBoot.context == null) Swagger2SpringBoot.context = servletContext;
        super.onStartup(servletContext);
    }
	
	@Bean
	public FilterRegistrationBean filtroJWT() {
		FilterRegistrationBean frb = new FilterRegistrationBean();
		frb.setFilter(new TokenFilter());
		frb.addUrlPatterns("/greenws/*");
		return frb;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Swagger2SpringBoot.class, args);
	}
}
