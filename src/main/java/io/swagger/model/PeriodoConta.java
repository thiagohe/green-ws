package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import br.com.mv.foundation.deserializer.PeriodoContaDeserializer;

@JsonDeserialize(using = PeriodoContaDeserializer.class)
public class PeriodoConta {
	
	@JsonProperty("dtInicioConta")
	private String dtInicioConta;
	
	@JsonProperty("dtFinalConta")
	private String dtFinalConta;

	public String getDtInicioConta() {
		return dtInicioConta;
	}

	public void setDtInicioConta(String dtInicioConta) {
		this.dtInicioConta = dtInicioConta;
	}

	public String getDtFinalConta() {
		return dtFinalConta;
	}

	public void setDtFinalConta(String dtFinalConta) {
		this.dtFinalConta = dtFinalConta;
	}
	
	
}
