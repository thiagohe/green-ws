package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.mv.foundation.deserializer.AtendimentoDeserializer;
import io.swagger.annotations.ApiModelProperty;


@JsonDeserialize(using = AtendimentoDeserializer.class)
public class Atendimento {
	
	@JsonProperty("cdAtendimento")
	private Long cdAtendimento;
	
	@JsonProperty("cdPaciente")
	private Long cdPaciente;
	
	@JsonProperty("tpAtendimento")
	private String tpAtendimento;
	
	@JsonProperty("cdConvenio")
	private Long cdConvenio;
	
	@JsonProperty("cdConPlan")
	private Long cdConPlan;
	
	@JsonProperty("cdSubPlano")
	private Long cdSubPlano;
	
	@JsonProperty("cdCid")
	private Long cdCid;
	
	@JsonProperty("cdProcedimento")
	private Long cdProcedimento;
	
	@JsonProperty("nrGuia")
	private String nrGuia;
	
	@JsonProperty("senhaSus")
	private String senhaSus;
	
	@JsonProperty("hrAtendimento")
	private String hrAtendimento;
	
	@JsonProperty("nrCpf")
	private String nrCpf;
	
	@JsonProperty("nmPaciente")
	private String nmPaciente;
	
	@JsonProperty("nrIdentidade")
	private String nrIdentidade;
	
	@JsonProperty("nmMae")
	private String nmMae;
	
	@JsonProperty("dtNascimento")
	private String dtNascimento;
	
	@JsonProperty("nrCarteira")
	private String nrCarteira;
	
	@JsonProperty("nmPrestador")
	private String nmPrestador;
	
	@JsonProperty("nmConvenio")
	private String nmConvenio;
	
	@JsonProperty("dsConPla")
	private String dsConPla;
	
	@JsonProperty("nmResponsavel")
	private String nmResponsavel;
	
	@JsonProperty("nrCns")
	private String nrCns;
	
	
	@ApiModelProperty(value = "Código do atendimento")
	public Long getCdAtendimento() {
		return cdAtendimento;
	}

	public void setCdAtendimento(Long cdAtendimento) {
		this.cdAtendimento = cdAtendimento;
	}

	@ApiModelProperty(value = "Código do paciente")
	public Long getCdPaciente() {
		return cdPaciente;
	}

	public void setCdPaciente(Long cdPaciente) {
		this.cdPaciente = cdPaciente;
	}

	public String getTpAtendimento() {
		return tpAtendimento;
	}

	public void setTpAtendimento(String tpAtendimento) {
		this.tpAtendimento = tpAtendimento;
	}

	public Long getCdConvenio() {
		return cdConvenio;
	}

	public void setCdConvenio(Long cdConvenio) {
		this.cdConvenio = cdConvenio;
	}

	public Long getCdConPlan() {
		return cdConPlan;
	}

	public void setCdConPlan(Long cdConPlan) {
		this.cdConPlan = cdConPlan;
	}

	public Long getCdSubPlano() {
		return cdSubPlano;
	}

	public void setCdSubPlano(Long cdSubPlano) {
		this.cdSubPlano = cdSubPlano;
	}

	public Long getCdCid() {
		return cdCid;
	}

	public void setCdCid(Long cdCid) {
		this.cdCid = cdCid;
	}

	public Long getCdProcedimento() {
		return cdProcedimento;
	}

	public void setCdProcedimento(Long cdProcedimento) {
		this.cdProcedimento = cdProcedimento;
	}

	public String getNrGuia() {
		return nrGuia;
	}

	public void setNrGuia(String nrGuia) {
		this.nrGuia = nrGuia;
	}

	public String getSenhaSus() {
		return senhaSus;
	}

	public void setSenhaSus(String senhaSus) {
		this.senhaSus = senhaSus;
	}

	public String getHrAtendimento() {
		return hrAtendimento;
	}

	public void setHrAtendimento(String hrAtendimento) {
		this.hrAtendimento = hrAtendimento;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}

	public String getNrIdentidade() {
		return nrIdentidade;
	}

	public void setNrIdentidade(String nrIdentidade) {
		this.nrIdentidade = nrIdentidade;
	}

	public String getNmMae() {
		return nmMae;
	}

	public void setNmMae(String nmMae) {
		this.nmMae = nmMae;
	}

	public String getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getNrCarteira() {
		return nrCarteira;
	}

	public void setNrCarteira(String nrCarteira) {
		this.nrCarteira = nrCarteira;
	}

	public String getNmPrestador() {
		return nmPrestador;
	}

	public void setNmPrestador(String nmPrestador) {
		this.nmPrestador = nmPrestador;
	}

	public String getNmConvenio() {
		return nmConvenio;
	}

	public void setNmConvenio(String nmConvenio) {
		this.nmConvenio = nmConvenio;
	}

	public String getDsConPla() {
		return dsConPla;
	}

	public void setDsConPla(String dsConPla) {
		this.dsConPla = dsConPla;
	}
	

	public String getNmResponsavel() {
		return nmResponsavel;
	}

	public void setNmResponsavel(String nmResponsavel) {
		this.nmResponsavel = nmResponsavel;
	}
	
	
	
	public String getNrCns() {
		return nrCns;
	}

	public void setNrCns(String nrCns) {
		this.nrCns = nrCns;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cdAtendimento);
	}
	
	
	
}