package br.com.mv.foundation.controller;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import br.com.mv.integra.compiler.parser.ParseException;
import br.com.mv.integra.framework.FachadaFramework;
import br.com.mv.integra.framework.exception.ChaveNaoExistenteException;
import br.com.mv.integra.framework.exception.InternacionalizacaoNaoEncontradaException;
import br.com.mv.integra.framework.exception.NenhumJSONenviadoException;
import br.com.mv.integra.framework.exception.NenhumRegistroEncontradoException;
import br.com.mv.integra.framework.exception.NenhumaConsultaFornecidaException;
import br.com.mv.integra.framework.exception.VersaoInvalidaException;
import io.swagger.Swagger2SpringBoot;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GenericController<T> {

	private T t;
	private static FachadaFramework fachada;

	public GenericController(){

	}

	private static FachadaFramework getFachada()
			throws InternacionalizacaoNaoEncontradaException, VersaoInvalidaException {
		if (GenericController.fachada == null)
			GenericController.fachada = new FachadaFramework(1, new Locale("pt", "BR"), Swagger2SpringBoot.logger);
		return GenericController.fachada;
	}

	public T get(String tableName, HashMap<String, Object> whereParam, Class<T> classe) throws JsonParseException, JsonMappingException, IOException, NenhumJSONenviadoException, NenhumRegistroEncontradoException, NenhumaConsultaFornecidaException, ChaveNaoExistenteException, ParseException, VersaoInvalidaException, InternacionalizacaoNaoEncontradaException, InstantiationException, IllegalAccessException{
		String json = "";
		
		T returnObject = getInstanceOfT(classe);
		Iterator mapIterator = whereParam.entrySet().iterator();
		
		String where = "";
		
		 while (mapIterator.hasNext()){
			 Map.Entry mapEntry = (Map.Entry) mapIterator.next();
		     String keyValue = (String) mapEntry.getKey();
		     Object value = mapEntry.getValue();
		     
		     if(value instanceof String){
		    	 if(!mapIterator.hasNext()){
			    	 where += "\""+ keyValue +"\": \""+ (String) value +"\"";
			     }else{
			    	 where += "\""+ keyValue +"\": \""+ (String) value +"\", ";
			     } 
		     }else if(value instanceof Integer){
		    	 if(!mapIterator.hasNext()){
			    	 where += "\""+ keyValue +"\": "+ (Integer) value +"";
			     }else{
			    	 where += "\""+ keyValue +"\": "+ (Integer) value +", ";
			     } 
		     }else if(value == null){
		    	 if(!mapIterator.hasNext()){
		    		 where += "\""+ keyValue +"\": \"null\"";
			     }else{
			    	 where += "\""+ keyValue +"\": \"null\", ";
			     }
		     }
		     
		     
		 }

		if((whereParam.size() > 0 && !where.equals("")) || (whereParam.size() == 0)){
			json = GenericController.getFachada().getFramework()
					.find("{\""+ tableName +"\":{"+ where +"}}");
			
			returnObject = (T) new ObjectMapper().readValue(json, classe);
		}

		return returnObject;
	}
	
	public T getInstanceOfT(Class<T> aClass) throws InstantiationException, IllegalAccessException
    {
       return aClass.newInstance();
    }      

}
