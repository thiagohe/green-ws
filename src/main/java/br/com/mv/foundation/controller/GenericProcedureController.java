package br.com.mv.foundation.controller;

import java.sql.Types;
import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class GenericProcedureController extends StoredProcedure{
	
	private static final String SQL = "dbamv.pkg_mv2000.atribui_empresa(1); DBAMV.PKG_MGCO_AUTORIZA_OC_SC.PRC_AUT_SC_OC";
	
	public GenericProcedureController(DataSource dataSource){
		super(dataSource, SQL);
		declareParameter(new SqlParameter("pTp_doc", Types.VARCHAR));
		declareParameter(new SqlParameter("pCd_doc", Types.VARCHAR));
		declareParameter(new SqlParameter("pTp_acao", Types.VARCHAR));
		declareParameter(new SqlParameter("pCd_user", Types.VARCHAR));
		declareParameter(new SqlOutParameter("pSit_Resultado", Types.VARCHAR));
		declareParameter(new SqlOutParameter("pCd_Resultado", Types.VARCHAR));
		declareParameter(new SqlOutParameter("pMsg_Resultado", Types.VARCHAR));
	}
	
	public void executar(HashMap<String, Object> parametros){
		execute(parametros);
	}
}
