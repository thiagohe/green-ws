package br.com.mv.foundation.controller;

import java.io.IOException;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mv.integra.compiler.parser.ParseException;
import br.com.mv.integra.framework.FachadaFramework;
import br.com.mv.integra.framework.exception.ChaveNaoExistenteException;
import br.com.mv.integra.framework.exception.InternacionalizacaoNaoEncontradaException;
import br.com.mv.integra.framework.exception.NenhumJSONenviadoException;
import br.com.mv.integra.framework.exception.NenhumRegistroEncontradoException;
import br.com.mv.integra.framework.exception.NenhumaConsultaFornecidaException;
import br.com.mv.integra.framework.exception.VersaoInvalidaException;
import io.swagger.Swagger2SpringBoot;

public class GenericSqlController<T> {

	private T t;
	private static FachadaFramework fachada;

	public GenericSqlController(){

	}

	private static FachadaFramework getFachada()
			throws InternacionalizacaoNaoEncontradaException, VersaoInvalidaException {
		if (GenericSqlController.fachada == null)
			GenericSqlController.fachada = new FachadaFramework(1, new Locale("pt", "BR"), Swagger2SpringBoot.logger);
		return GenericSqlController.fachada;
	}
	

	public T get(String sql, Class<T> classe) throws JsonParseException, JsonMappingException, IOException, NenhumJSONenviadoException, NenhumRegistroEncontradoException, NenhumaConsultaFornecidaException, ChaveNaoExistenteException, ParseException, VersaoInvalidaException, InternacionalizacaoNaoEncontradaException, InstantiationException, IllegalAccessException{
		String json = "";
		
		T returnObject = getInstanceOfT(classe);
		
			json = GenericSqlController.getFachada().getFramework()
					.find("{\"MVQL:QUERY\":\""+ sql +"\"}");
			
			returnObject = (T) new ObjectMapper().readValue(json, classe);

		return returnObject;
	}
	
	public T call(String sql, Class<T> classe) throws JsonParseException, JsonMappingException, IOException, NenhumJSONenviadoException, NenhumRegistroEncontradoException, NenhumaConsultaFornecidaException, ChaveNaoExistenteException, ParseException, VersaoInvalidaException, InternacionalizacaoNaoEncontradaException, InstantiationException, IllegalAccessException{
		String json = "";
		
		T returnObject = getInstanceOfT(classe);
		
			json = GenericSqlController.getFachada().getFramework()
					.call("{\"MVQL:CALL\":"+ sql +"}");
			
			returnObject = (T) new ObjectMapper().readValue(json, classe);

		return returnObject;
	}
	
	public T getInstanceOfT(Class<T> aClass) throws InstantiationException, IllegalAccessException
    {
       return aClass.newInstance();
    }      

}
