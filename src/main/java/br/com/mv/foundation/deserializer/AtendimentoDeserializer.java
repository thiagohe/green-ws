package br.com.mv.foundation.deserializer;

import java.io.IOException;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;

import io.swagger.model.Atendimento;


public class AtendimentoDeserializer extends StdDeserializer<Atendimento> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AtendimentoDeserializer() { 
		this(null); 
	} 

	public AtendimentoDeserializer(Class<?> vc) { 
		super(vc); 
	}

	@Override
	public Atendimento deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		Atendimento atendimento = null;
		
		try {
			JsonNode node = jp.readValueAsTree(); //elemento raiz
			
			if(node.get("MVQL:QUERY") != null){
				ArrayNode internJson = (ArrayNode) node.get("MVQL:QUERY"); //lista de retornos
				
				Iterator<JsonNode> internIterator = internJson.elements();
				
				if (internIterator.hasNext()) {
					atendimento = new Atendimento();
					JsonNode jsonNode = internIterator.next();
					
					atendimento.setCdAtendimento(jsonNode.get("CD_ATENDIMENTO")==null || jsonNode.get("CD_ATENDIMENTO").asText(null)==null? null : jsonNode.get("CD_ATENDIMENTO").asLong());
					
					atendimento.setCdPaciente(jsonNode.get("CD_PACIENTE")==null || jsonNode.get("CD_PACIENTE").asText(null)==null? null : jsonNode.get("CD_PACIENTE").asLong());
					
					atendimento.setTpAtendimento( jsonNode.get("TP_ATENDIMENTO")==null ? null : jsonNode.get("TP_ATENDIMENTO").asText(null) );
					
					atendimento.setCdConvenio(jsonNode.get("CD_CONVENIO")==null || jsonNode.get("CD_CONVENIO").asText(null)==null? null : jsonNode.get("CD_CONVENIO").asLong());
					
					atendimento.setCdConPlan(jsonNode.get("CD_CON_PLA")==null || jsonNode.get("CD_CON_PLA").asText(null)==null? null : jsonNode.get("CD_CON_PLA").asLong());
					
					atendimento.setCdSubPlano(jsonNode.get("CD_SUB_PLANO")==null || jsonNode.get("CD_SUB_PLANO").asText(null)==null? null : jsonNode.get("CD_SUB_PLANO").asLong());
					
					atendimento.setCdCid(jsonNode.get("CD_CID")==null || jsonNode.get("CD_CID").asText(null)==null? null : jsonNode.get("CD_CID").asLong());
					
					atendimento.setCdProcedimento(jsonNode.get("CD_PROCEDIMENTO")==null || jsonNode.get("CD_PROCEDIMENTO").asText(null)==null? null : jsonNode.get("CD_PROCEDIMENTO").asLong());
					
					atendimento.setNrGuia( jsonNode.get("NR_GUIA")==null ? null : jsonNode.get("NR_GUIA").asText(null) );
					
					atendimento.setSenhaSus( jsonNode.get("SENHA_SUS")==null ? null : jsonNode.get("SENHA_SUS").asText(null) );
					
					atendimento.setHrAtendimento( jsonNode.get("HR_ATENDIMENTO")==null ? null : jsonNode.get("HR_ATENDIMENTO").asText(null) );
					
					atendimento.setNrCpf(jsonNode.get("NR_CPF")==null ? null : jsonNode.get("NR_CPF").asText(null) );
					
					atendimento.setNmPaciente(jsonNode.get("NM_PACIENTE")==null ? null : jsonNode.get("NM_PACIENTE").asText(null));
					
					atendimento.setNrIdentidade(jsonNode.get("NR_IDENTIDADE")==null ? null : jsonNode.get("NR_IDENTIDADE").asText(null));
					
					atendimento.setNmMae(jsonNode.get("NM_MAE")==null ? null : jsonNode.get("NM_MAE").asText(null));
					
					atendimento.setDtNascimento(jsonNode.get("DT_NASCIMENTO")==null ? null : jsonNode.get("DT_NASCIMENTO").asText(null));
					
					atendimento.setNrCarteira(jsonNode.get("NR_CARTEIRA")==null ? null : jsonNode.get("NR_CARTEIRA").asText(null));
					
					atendimento.setNmPrestador(jsonNode.get("NM_PRESTADOR")==null ? null : jsonNode.get("NM_PRESTADOR").asText(null));
					
					atendimento.setNmConvenio(jsonNode.get("NM_CONVENIO")==null ? null : jsonNode.get("NM_CONVENIO").asText(null));
					
					atendimento.setDsConPla(jsonNode.get("DS_CON_PLA")==null ? null : jsonNode.get("DS_CON_PLA").asText(null));
					
					atendimento.setNmResponsavel(jsonNode.get("NM_RESPONSAVEL")==null ? null : jsonNode.get("NM_RESPONSAVEL").asText(null));
					
					atendimento.setNrCns(jsonNode.get("NR_CNS")==null ? null : jsonNode.get("NR_CNS").asText(null));
				}
				
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return atendimento;
	}
	
	
}
