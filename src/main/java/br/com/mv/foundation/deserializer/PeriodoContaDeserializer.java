package br.com.mv.foundation.deserializer;

import java.io.IOException;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;

import io.swagger.model.PeriodoConta;

public class PeriodoContaDeserializer extends StdDeserializer<PeriodoConta> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PeriodoContaDeserializer() {
		this(null);
	}

	public PeriodoContaDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public PeriodoConta deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		PeriodoConta periodoConta = null;

		try {
			JsonNode node = jp.readValueAsTree(); // elemento raiz

			if (node.get("MVQL:QUERY") != null) {
				ArrayNode internJson = (ArrayNode) node.get("MVQL:QUERY"); 

				Iterator<JsonNode> internIterator = internJson.elements();

				if (internIterator.hasNext()) {
					JsonNode jsonNode = internIterator.next();
					periodoConta = new PeriodoConta();
					periodoConta.setDtInicioConta(jsonNode.get("DT_INICIO_CONTA")==null ? null : jsonNode.get("DT_INICIO_CONTA").asText(null));
					periodoConta.setDtFinalConta(jsonNode.get("DT_FINAL_CONTA")==null ? null : jsonNode.get("DT_FINAL_CONTA").asText(null));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return periodoConta;
	}
}
