package br.com.mv.foundation.util;

public class ConsultaQuery {
	
	public static String getAtendimento(Long codigoAtendimento, Long codigoPaciente) {
		StringBuilder sb = new StringBuilder("SELECT ");
		sb.append(" atendime.cd_atendimento, ");
		sb.append(" atendime.cd_paciente, ");
		sb.append(" atendime.tp_atendimento, ");
		sb.append(" atendime.cd_convenio, ");
		sb.append(" atendime.cd_con_pla, ");
		sb.append(" atendime.cd_sub_plano, ");
		sb.append(" atendime.cd_cid, ");
		sb.append(" atendime.cd_pro_int as cd_procedimento,  ");
		sb.append(" atendime.nr_guia,  ");
		sb.append(" atendime.senha_sus,  ");
		sb.append(" TO_CHAR(atendime.hr_atendimento, 'dd-MM-yyyy') AS hr_atendimento,  ");
		sb.append(" paciente.nr_cpf,  ");
		sb.append(" paciente.nm_paciente,  ");
		sb.append(" paciente.nr_identidade, ");
		sb.append(" paciente.nm_mae, ");
		sb.append(" TO_CHAR(paciente.dt_nascimento,'dd-MM-yyyy') AS dt_nascimento, ");
		sb.append(" c.nr_carteira, ");
		sb.append(" m.nm_prestador, ");
		sb.append(" convenio.nm_convenio, ");
		sb.append(" con_pla.ds_con_pla, ");
		sb.append(" r.nm_responsavel, ");
		sb.append(" paciente.nr_cns ");
		sb.append(" FROM dbamv.atendime atendime, dbamv.paciente paciente,dbamv.carteira c, dbamv.prestador m, dbamv.convenio convenio, dbamv.con_pla con_pla, dbamv.responsa r  ");
		sb.append(" WHERE ");
		
		if (codigoAtendimento!=null) {
			sb.append(" atendime.cd_atendimento = ").append(codigoAtendimento);
		}
		else if (codigoPaciente!=null) {
			sb.append(" atendime.cd_paciente = ").append(codigoPaciente);
		}
		
		sb.append(" AND atendime.cd_convenio = convenio.cd_convenio(+) ");
		sb.append(" AND atendime.cd_convenio = con_pla.cd_convenio(+)  ");
		sb.append(" AND atendime.cd_con_pla = con_pla.cd_con_pla(+)  ");
		sb.append(" AND atendime.cd_paciente = paciente.cd_paciente(+)  ");
		sb.append(" AND atendime.cd_paciente = c.cd_paciente(+)  ");
		sb.append(" AND atendime.cd_convenio = c.cd_convenio(+)  ");
		sb.append(" AND atendime.cd_prestador = m.cd_prestador(+)  ");
		sb.append(" AND atendime.cd_atendimento = r.cd_atendimento(+) AND rownum = 1");
		
		return sb.toString();
	}
	
	public static String getPeriodoConta(Long codigoAtendimento, Long numeroConta) {
		StringBuilder sb = new StringBuilder("  SELECT dt_inicio_conta, dt_final_conta FROM ( ");
		sb.append(" SELECT TO_CHAR(reg_fat.dt_inicio, 'dd-MM-yyyy') AS dt_inicio_conta, ");
		sb.append(" TO_CHAR(reg_fat.dt_final, 'dd-MM-yyyy') AS dt_final_conta ");
		sb.append(" FROM reg_fat INNER JOIN atendime ON reg_fat.cd_atendimento = atendime.cd_atendimento  ");
		sb.append(" WHERE reg_fat.cd_atendimento = ").append(codigoAtendimento);
		sb.append(" AND reg_fat.cd_reg_fat = ").append(numeroConta);
		sb.append(" UNION ");
		sb.append(" SELECT TO_CHAR(atendime.dt_atendimento, 'dd-MM-yyyy') AS dt_inicio_conta, TO_CHAR(atendime.dt_atendimento, 'dd-MM-yyyy') AS dt_final_conta ");
		sb.append(" FROM itreg_amb INNER JOIN atendime ON atendime.cd_atendimento = itreg_amb.cd_atendimento  ");
		sb.append(" WHERE itreg_amb.cd_atendimento = ").append(codigoAtendimento);
		sb.append(" AND itreg_amb.CD_REG_AMB = ").append(numeroConta);
		sb.append(" UNION ");
		sb.append(" SELECT TO_CHAR(atendime.dt_atendimento, 'dd-MM-yyyy') AS dt_inicio_conta, TO_CHAR(atendime.dt_atendimento, 'dd-MM-yyyy') AS dt_final_conta  ");
		sb.append(" FROM eve_siasus INNER JOIN atendime ON atendime.cd_atendimento = eve_siasus.cd_atendimento ");
		sb.append(" WHERE eve_siasus.cd_atendimento = ").append(codigoAtendimento);
		sb.append(" AND eve_siasus.cd_atendimento = ").append(numeroConta);
		sb.append(" ) WHERE rownum = 1 ");
		return sb.toString();
	}
	
}
